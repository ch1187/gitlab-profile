```mermaid
%%{init: {'securityLevel': 'loose', 'theme': 'base', 'themeVariables': { 'darkMode': false, 'primaryColor': '#ffaacc', 'fontSize': '14px', 'lineColor': '#ffffff', 'edgeLabelBackground':'#ffffaa'}}}%%
flowchart LR
 id1>Nutzbare  Lokale  Klimainformationen für Deutschland \n Useful local climate information for Germany ]
 id2["<img src='https://ch1187.gitlab-pages.dkrz.de/Information/_static/logo.png' width='700px' height='60px'/>"]
 style id1 fill:#55ff33,stroke:#333,stroke-width:2px
 style id2 fill:#ffffff,stroke:#ffffff,stroke-width:4px
 id2 --> id1
 linkStyle default stroke:white,stroke-width:2px;
```

Das Projekt Nukleus wird in seiner zweiten Phase einen starken Fokus auf die Bereitstellung und Nutzbarmachung der mit einem Gitterpunktsabstand von ca. 3 km sehr hochaufgelösten Klimasimulationen über Deutschland legen. Hierbei wird das DKRZ über seine Freva-Plattform die wissenschaftlichen Auswerteroutinen zur Verfügung stellen, deren Ergebnisse neben den Projektpartnern auch in einem Klima-Kataster der Öffentlichkeit zur Verfügung gestellt werden.

## Project Plan:

```mermaid
gantt
dateFormat  YYYY-MM-DD
axisFormat  %Y-%m
tickInterval 3month
section AP1
AP1 – Koordination und prototypische Umsetzung eines Klimakatasters (Leitung-Hereon-GERICS):2023-06-01,2026-06-01
section A1.1 Hereon-GERICS 6PM, KIT 9PM
M1.1:2023-12-01,2024-03-01
M1.1:2024-12-01,2025-03-01
M1.1:2025-12-01,2026-03-01
M1.1:2026-03-01,2026-05-30
section A1.2 Hereon-GERICS 12PM, DKRZ 8PM, TUD 3PM
M1.2:2025-03-01,2025-06-01
M1.2:2025-09-01,2025-12-01
section A1.3 Hereon-GERICS 4PM,DKRZ 28PM
M1.4:2026-03-01,2026-05-30
section A1.4 Hereon-GERICS 2PM,KIT 9PM, TUD 2PM
M1.3:2025-09-01,2025-12-01
M3.1:2025-09-01,2025-12-01
M3.3:2025-09-01,2025-12-01
section AP2
AP2 – Evaluierung und Qualitätssicherung der Klimadaten für die Wirkmodelle (Leitung-KIT) :2023-06-01,2026-06-01
section A2.1 KIT 18PM, UW 9PM, BTU 12PM
M2.3:2026-03-12,2026-05-30
section A2.2 Hereon-KS 12PM, KIT 18PM, UW 12PM, BTU 9PM 
M2.1:2024-09-01,2024-12-01
M2.1:2025-09-01,2025-12-01
section A2.3 UW 10PM, JLU 25PM 
M2.2:2025-03-01,2025-06-01
M2.2:2025-09-01,2025-12-01
section AP3
AP3 –Optimierung und Standardisierung der Schnittstellen zu Wirkmodellen (Leitung-TUD):2023-06-01,2026-06-01
section A3.1 Hereon-GERICS 4PM, DKRZ 2PM, UW 3PM TUD 2PM 
M3.2:2025-09-01,2025-12-01
section A3.2 KIT 8PM, DKRZ 12PM, UW 2PM, BTU 24PM, JLU 4PM 
M3.1:2023-09-01,2023-12-01
M3.1:2024-09-01,2024-12-01
M3.1:2025-03-01,2025-06-01
M3.1:2026-03-01,2026-05-30
section A3.3 KIT 2PM, TUD 23PM 
M3.3:2025-12-01,2026-03-01
M1.3:2025-12-01,2026-03-01
section A3.4 Hereon-GERICS 14PM, KIT 8PM, DKRZ 4PM, TUD 6PM 
M3.1:2024-09-01,2024-12-01
M1.3:2025-09-01,2025-12-01
M3.1:2026-03-01,2026-05-30
```

* Reference Table of Milestones:

| **Milestone** | **Description** | **TASK** |
|---------------|-----------------|----------|
| **M1.1** | Berichtswesen und Projekttreffen | A1.1 |
| **M1.2** | Analysen und Klimakatasterkonzept | A1.2 |
| **M1.3** | White Paper, Leitfäden und Tutorials | A1.4, A3.3, A3.4 |
| **M1.4** | Prototyp des Klimakatasters | A1.3 |
| **M2.1** | Die Robustheit der GWL-Signale Ergänzung des Ensembles | A2.2 |
| **M2.2** | Bias-korrigierte Daten der konvektionserlaubenden Simulationen | A2.3 |
| **M2.3** | Qualitäts- und Bandbreitenanalyse und Mehrwert der hohen Auflösung | A2.1 |
| **M3.1** | Freva-Plugins | A3.2, A3.4, A1.4 |
| **M3.2** | Integration neuer Datensätze | A3.1 |
| **M3.3** | Bericht zu Unsicherheiten in der Modellkette | A3.3, A1.4 |



<!-- TODO: 
1. How many dataset this project supports
2. A diagram based on the Name of projects
3. A pie chart which shows the finished, broken, and sheduled jobs
4. A Diagram or Pie chart to show most used plugins
5. A Diagram or Pie chart to Most used data
6. Institutes rank in using jobs
7. People rank in using jobs
8. Programming languages percentage in plugins
9. Institutes rank in group activities in Gitlab
10. Peoples rank in group activities in Gitlab
11. Status of plugins pipelines and their status in project -->
